Daß die Elektrodynamik Maxwells — wie dieselbe gegen¬
wärtig aufgefaßt zu werden pflegt — in ihrer Anwendung auf
bewegte Körper zu Asymmetrien führt, welche den Phänomenen
nicht anzuhaften scheinen, ist bekannt. Man denke z. B. an
die elektrodynamische Wechselwirkung zwischen einem Mag¬
neten und einem Leiter. Das beobachtbare Phänomen hängt
hier nur ab von der Relativbewegung von Leiter und Magnet,
während nach der üblichen Auffassung die beiden Eälle, daß
der eine oder der andere dieser Körper der bewegte sei, streng
voneinander zu trennen sind. Bewegt sich nämlich der Magnet
und ruht der Leiter, so entsteht in der Umgebung des Magneten
ein elektrisches Feld von gewissem Energiewerte, welches an
den Orten, wo sich Teile des Leiters befinden, einen Strom
erzeugt. Ruht aber der Magnet und bewegt sich der Leiter,
so entsteht in der Umgebung des Magneten kein elektrisches
Feld, dagegen im Leiter eine elektromotorische Kraft, welcher
an sich keine Energie entspricht, die aber — Gleichheit der
Relativbewegung bei den beiden ins Auge gefaßten Fällen
vorausgesetzt — zu elektrischen Strömen von derselben Größe
und demselben Verlaufe Veranlassung gibt, wie im ersten Falle
die elektrischen Kräfte.
